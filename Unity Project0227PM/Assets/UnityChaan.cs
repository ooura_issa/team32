﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityChaan : MonoBehaviour
{
    Rigidbody2D rigidplayer;
    public float speed = 3.0f;
    public float jumpForce = 250.0f;
    Animator anim;
    public LayerMask groundLayer;
	// Use this for initialization
	void Start ()
    {
        rigidplayer = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        float h= Input.GetAxis("Horizontal");
        rigidplayer.velocity = new Vector2(speed * h, rigidplayer.velocity.y);
        anim.SetFloat("Run", Mathf.Abs(h));
        RaycastHit2D hit = Physics2D.Linecast(transform.position,
            transform.position - (transform.up * 0.5f), groundLayer);
        if (hit.collider && Input.GetButtonDown("jump"))
        {
            rigidplayer.AddForce(Vector2.up * jumpForce);
        }
        if(h<0&&transform.localScale.x>0||
            h>0&&transform.localScale.x<0)
        {
            Vector2 pos = transform.localScale;
            pos.x *= -1;
            transform.localScale = pos;
        }
        if(h<0&&transform.localScale.x>0||h>0&&transform.localScale.x<0)
        {
            Vector2 pos = transform.localScale;
            pos.x *= -1;
            transform.localScale = pos;
        }

		
	}
}
